package challenge.episerver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Test;

/**
 * Class for testing functions of the developed software.
 * @author Igor Klepic.
 *
 */
public class LibraryTest {
	
	@Test
	public void getAllItemsTest() {
		Library data = new Library();
		int numberOfItems = 14;
		assertEquals(numberOfItems, data.getAllItems().size());
	}
	
	@Test
	public void getAllBooksTest() {
		Library data = new Library();	
		int numberOfBooks = 8;
		assertEquals(numberOfBooks, data.getAllBooks().size());
	}
	
	@Test
	public void getAllMagazinesTest() {
		Library data = new Library();	
		int numberOfMagazines = 6;
		assertEquals(numberOfMagazines, data.getAllMagazines().size());
	}
	
	@Test
	public void getItemByISBNTest() {
		Library data = new Library();
		//Expected values of the item.
		String ISBNnumber = "2145-8548-3325";
		String title = "Das gro�e GU-Kochbuch Kochen f�r Kinder";
		//There are two authors of this item - ferdinand and lieblich. walter is not.
		int numberOfAuthors = 2;
		Author ferdinand = data.getAuthorByEmail("pr-ferdinand@optivo.de");
		Author lieblich = data.getAuthorByEmail("pr-lieblich@optivo.de");
		Author walter = data.getAuthorByEmail("pr-walter@optivo.dee");
		//Return the item and perform tests.
		Item item = data.getItemByISBN(ISBNnumber);
		assertEquals(ISBNnumber, item.getISBN());
		assertEquals(title, item.getTitle());
		assertEquals(numberOfAuthors, item.getAuthors().size());
		assertTrue(item.getAuthors().contains(ferdinand));
		assertTrue(item.getAuthors().contains(lieblich));
		assertFalse(item.getAuthors().contains(walter));
	}
	
	@Test
	public void getAuthorByEmailTest() {
		Library data = new Library();
		//Expected values of the author.
		String email = "pr-ferdinand@optivo.de";
		String firstName = "Franz";
		String lastName = "Ferdinand";
		//Return the author and perform tests.
		Author author = data.getAuthorByEmail(email);
		assertEquals(email, author.getEmail());
		assertEquals(firstName, author.getFirstName());
		assertEquals(lastName, author.getLastName());
	}
	
	@Test
	public void getItemsListOfAuthorTest() {
		Library data = new Library();
		//Select the author.
		String authorEmail = "pr-lieblich@optivo.de";
		//There are four items of this author - three books and one magazine.
		//Last magazine does not belong to this author.
		int numberOfItems = 4;
		Book book1 = (Book)data.getItemByISBN("2145-8548-3325");
		Book book2 = (Book)data.getItemByISBN("2221-5548-8585");
		Book book3 = (Book)data.getItemByISBN("1024-5245-8584");
		Magazine magazine1 = (Magazine)data.getItemByISBN("2365-5632-7854");
		Magazine magazine2 = (Magazine)data.getItemByISBN("5454-5587-3210");
		//Return the list of items and perform tests.
		List<Item> itemsListOfAuthor = data.getItemsListOfAuthor(authorEmail);
		assertEquals(numberOfItems, itemsListOfAuthor.size());
		assertTrue(itemsListOfAuthor.contains(book1));
		assertTrue(itemsListOfAuthor.contains(book2));
		assertTrue(itemsListOfAuthor.contains(book3));
		assertTrue(itemsListOfAuthor.contains(magazine1));
		assertFalse(itemsListOfAuthor.contains(magazine2));
		
	}

}
