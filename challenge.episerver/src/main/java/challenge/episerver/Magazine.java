package challenge.episerver;

import java.util.List;

/**
 * Class representing a magazine which inherits title, ISBN number and list of authors attributes
 * from its parent Item and has an additional attribute publicationDate.
 * @author Igor Klepic.
 *
 */
public class Magazine extends Item {
	
	/*
	 * For purpose of this task just to keep it simple a publicationDate is represented as string.
	 * It could also be represented as some object used for representing dates. 
	 * This would be useful if we would like to sort magazines on the publicationDate attribute.
	 */
	private String publicationDate;
	
	/**
	 * Constructor for this class.
	 * @param title -> title of the book(inherited).
	 * @param ISBN -> unique ISBN number(inherited).
	 * @param authors -> list of authors of this item(inherited).
	 * @param publicationDate -> date when the magazine was published.
	 */
	public Magazine(String title, String ISBN, List<Author> authors, String publicationDate) {
		super(title, ISBN, authors);
		this.publicationDate = publicationDate;
	}
	
	
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	public String getPublicationDate() {
		return this.publicationDate;
	}
	
	
	public String toString() {
		return "MAGAZINE [" + super.toString() + ";  PublicationDate=" + this.getPublicationDate() + "]";
	}
	

}
