package challenge.episerver;

/**
 * Class representing an author with email, first name and last name.
 * @author Igor Klepic.
 *
 */
public class Author {
	
	private String email;
	private String firstName;
	private String lastName;
	
	/**
	 * Constructor for this class.
	 * @param email -> email of an author.
	 * @param firstName -> first name of the author.
	 * @param lastName -> last name of the author.
	 */
	public Author(String email, String firstName, String lastName) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return this.email;
	}
	

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFirstName() {
		return this.firstName;
	}
	
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName() {
		return this.lastName;
	}
	
	
	public String toString() {
		return "AUTHOR [Email=" + this.getEmail() + "; FirstName=" + this.getFirstName() + "; LastName=" + this.getLastName() + "]";
	}
	
	


}
