package challenge.episerver;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class containing information of the authors and the items(books and magazines).
 * @author Igor Klepic.
 *
 */
public class Library {
	
	/**
	 * Attribute which contains information of the authors. It is a map that maps
	 * email of the author into the Author's object.
	 */
	private Map<String, Author> authorsMap;
	
	/**
	 * Attribute which contains information of the items. It is a map that maps
	 * ISBN number of the item into the Item's object.
	 */
	private Map<String, Item> itemsMap;
	
	/**
	 * Constructor for this class.
	 * Reads the data from the csv files and initializes the attributes.
	 */
	public Library() {
		this.authorsMap = DataManagment.exportAuthors();
		this.itemsMap = DataManagment.exportItems(authorsMap);	
	}
	
	public Map<String, Author> getAuthorsMap() {
		return this.authorsMap;
	}
	
	public Map<String, Item> getItemsMap() {
		return this.itemsMap;
	}
	
	/**
	 * Gets all items.
	 * @return a list of all of the items. 
	 */
	public List<Item> getAllItems() {
		return new LinkedList<Item>(this.getItemsMap().values());
	}
	
	/**
	 * Gets all books.
	 * @return a list of all of the books.
	 */
	public List<Book> getAllBooks() {
		List<Book> booksList = new LinkedList<Book>();
		for (Item item: this.getAllItems()) {
			if (item instanceof Book) {
				booksList.add((Book)item);
			}
		}
		return booksList;
	}
	
	/**
	 * Gets all magazines.
	 * @return a list of all of the magazines.
	 */
	public List<Magazine> getAllMagazines() {
		List<Magazine> magazinesList = new LinkedList<Magazine>();
		for (Item item: this.getAllItems()) {
			if (item instanceof Magazine) {
				magazinesList.add((Magazine)item);
			}
		}
		return magazinesList;
	}
	
	/**
	 * Gets the item with the given ISBN number from the map of all of the items.
	 * @param ISBNnumber -> unique number which represents a particular item.
	 * @return item with the particular ISBN number.
	 */
	public Item getItemByISBN(String ISBNnumber) {
		return this.getItemsMap().get(ISBNnumber);
	}
	
	/**
	 * Gets the author with the given email from the map of all of the authors.
	 * @param email -> email of an author we want to get.
	 * @return author with the particular email.
	 */
	public Author getAuthorByEmail(String email) {
		return this.getAuthorsMap().get(email);
	}
	
	/**
	 * Gets the items of a given author from the list of all of the items.
	 * @param email -> email of the author.
	 * @return list of the items of the particular author.
	 */
	public List<Item> getItemsListOfAuthor(String email) {
		Author author = this.getAuthorByEmail(email);
		List<Item> itemsListOfAuthor = new LinkedList<Item>();
		for (Item item: this.getAllItems()) {
			if (item.getAuthors().contains(author)) {
				itemsListOfAuthor.add(item);
			}
		}
		return itemsListOfAuthor;
	}
	
	/**
	 * Sorts a map of the items by the value. Values are Item's
	 * objects and sorted by the title attribute.
	 * @return sorted list by the title of all of the items.
	 */
	public List<Item> getAllItemsSorted() {
		Map<String, Item> itemsMapSorted = this.getItemsMap().entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
		return new LinkedList<Item>(itemsMapSorted.values());
	}
	
	

}
