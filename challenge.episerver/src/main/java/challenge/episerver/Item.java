package challenge.episerver;

import java.util.List;

/**
 * Class representing an item(book or magazine) with title, ISBN number and list of authors.
 * Implement Comparable interface to be able to sort by the title of the items.
 * @author Igor Klepic.
 *
 */
public class Item implements Comparable<Item>{
	
	private String title;
	private String ISBN;
	private List<Author> authors;
	
	/**
	 * Constructor for this class.
	 * @param title -> title of the item.
	 * @param ISBN -> unique ISBN number.
	 * @param authors -> list of authors of this item.
	 */
	public Item(String title, String ISBN, List<Author> authors) {
		this.title = title;
		this.ISBN = ISBN;
		this.authors = authors;
	}
	
	
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getISBN() {
		return this.ISBN;
	}
	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}
	
	
	public List<Author> getAuthors() {
		return this.authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	
	
	/**
	 * Concatenates emails of all of the authors of this item into one string. 
	 * @return a single string with all of the emails separated by a comma.
	 */
	private String emailsAsString() {
		String emails = "";
		for (Author author: this.getAuthors()) {
			emails += author.getEmail();
			emails += ",";				
		}
		if (emails.length() > 0) {
			return emails.substring(0, emails.length() - 1);
		}
		else {
			return emails;
		}
		
	}
	
	
	public String toString() {
		return "Title=" + this.getTitle() + ";  ISBN=" + this.getISBN() + ";  Authors=" + this.emailsAsString();
	}
	
	
	/**
	 * Comparing two items by the title.
	 */
	public int compareTo(Item item) {
		return this.getTitle().compareTo(item.getTitle());
	}
	

}
