package challenge.episerver;

import java.util.List;

/**
 * Class representing a book which inherits title, ISBN number and list of authors attributes
 * from its parent Item and has an additional attribute description.
 * @author Igor Klepic.
 *
 */
public class Book extends Item {
	
	private String description;
	
	/**
	 * Constructor for this class.
	 * @param title -> title of the book(inherited).
	 * @param ISBN -> unique ISBN number(inherited).
	 * @param authors -> list of authors of this item(inherited).
	 * @param description -> short description of the book.
	 */
	public Book(String title, String ISBN, List<Author> authors, String description) {
		super(title, ISBN, authors);
		this.description = description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescription() {
		return this.description;
	}
	
	
	public String toString() {
		return "BOOK [" + super.toString() + ";  Description=" + this.getDescription() + "]";
	}

}
