package challenge.episerver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class containing helper methods to export the data and to print results.
 * @author Igor Klepic.
 *
 */
public class DataManagment {
	
	/**
	 * Reads the data of the authors from the csv file, creates corresponding Author
	 * objects and puts them into the map. 
	 * @return map of all the authors with an email as a key and an Author object as a value.
	 */
	public static Map<String, Author> exportAuthors() {
		
		String line = "";
		//A delimiter to separate different parts of data contained in line.
		String dataSplitBy = ";";
		boolean firstLine = true;
		//A map that maps author's email into the object of type Author. 
		Map<String, Author> authorsMap = new LinkedHashMap<String, Author>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("src/main/resources/autoren.csv"));
			while ((line = br.readLine()) != null) {
				//First line is a header so we skip it.
				if (firstLine) {
					firstLine = false;
					continue;
				}
				//Splitting the line to get the data and creating an Author object.
				String[] data = line.split(dataSplitBy);
				Author author = new Author(data[0], data[1], data[2]);
				//Object is put into the map with email as a mapping key.
				authorsMap.put(author.getEmail(), author);
			}		
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		return authorsMap;		
	}
	
	/**
	 * Reads the data of the items from the csv file, creates corresponding Item
	 * objects and puts them into the map. 
	 * @param authorsMap -> map of all of the authors.
	 * @return map of all the items with an ISBN number as a key and Item object as a value.
	 */
	public static Map<String, Item> exportItems(Map<String, Author> authorsMap) {
		
		String line = "";
		//A delimiter to separate different parts of data contained in line.
		String dataSplitBy = ";";
		//A delimiter to separate emails of all of the authors of this item.
		String emailSplitBy = ",";
		boolean firstLine = true;
		//A map that maps item's ISBN number into the object of type Item. 
		Map<String, Item> itemsMap = new LinkedHashMap<String, Item>();
		
		BufferedReader br = null;
		try {
			//First we read csv file containing information of all the books.
			br = new BufferedReader(new FileReader("src/main/resources/buecher.csv"));
			while ((line = br.readLine()) != null) {
				//First line is a header so we skip it.
				if (firstLine) {
					firstLine = false;
					continue;
				}
				//Splitting the line to get the data of the particular book.
				String[] data = line.split(dataSplitBy);
				//Splitting to get all the emails of the authors of this book.
				String[] bookAuthorsEmails = data[2].split(emailSplitBy);
				//Creating list of all the authors of this book.
				List<Author> bookAuthors = new LinkedList<Author>();
				for (String email: bookAuthorsEmails) {
					//We use mapping to get the Author object.
					Author bookAuthor = authorsMap.get(email);
					if (bookAuthor != null) {
						bookAuthors.add(bookAuthor);
					}				
				}
				//Creating a Book object with all the data from the file.
				Book book = new Book(data[0], data[1], bookAuthors, data[3]);
				//Object is put into the map with ISBN number as a mapping key.
				itemsMap.put(book.getISBN(), book);
			}
			br.close();
			
			//We do the same for the magazines.
			line = "";
			firstLine = true;
			br = new BufferedReader(new FileReader("src/main/resources/zeitschriften.csv"));
			while ((line = br.readLine()) != null) {
				if (firstLine) {
					firstLine = false;
					continue;
				}
				String[] data = line.split(dataSplitBy);			
				String[] magazineAuthorsEmails = data[2].split(emailSplitBy);
				List<Author> magazineAuthors = new LinkedList<Author>();
				for (String email: magazineAuthorsEmails) {
					Author magazineAuthor = authorsMap.get(email);
					if (magazineAuthor != null) {
						magazineAuthors.add(magazineAuthor);
					}				
				}					
				Magazine magazine = new Magazine(data[0], data[1], magazineAuthors, data[3]);
				itemsMap.put(magazine.getISBN(), magazine);
			}
			br.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return itemsMap;	
	}
	
	/**
	 * Prints a String representation of an item by using it's toString() method.
	 * Works also for the objects of type Book and Magazine.
	 * @param item -> Item object which we print out. 
	 */
	public static void printItem(Item item) {
		if (item == null) {
			System.out.println("Item does not exsist!");
		}
		else {
			System.out.println(item.toString());
		}
	}
	
	/**
	 * Prints a String representation of all of the items in a list.
	 * @param list -> list of items which we print out.
	 */
	public static void printItems(List<Item> itemsList) {
		for (Item item: itemsList) {
			printItem(item);
		}
	}
	
	/**
	 * Prints a String representation of all of the books in a list.
	 * @param list -> list of books which we print out.
	 */
	public static void printBooks(List<Book> booksList) {
		for (Book book: booksList) {
			printItem(book);
		}
	}
	
	/**
	 * Prints a String representation of all of the magazines in a list.
	 * @param list -> list of magazines which we print out.
	 */
	public static void printMagazines(List<Magazine> magazinesList) {
		for (Magazine magazine: magazinesList) {
			printItem(magazine);
		}
	}
}
