package challenge.episerver;

import java.util.List;

/**
 * Main method to execute individual functions of the software and to print results.
 * @author Igor Klepic.
 *
 */
public class MethodMain {
	
	
	public static void main(String[] args) {
			
		Library data = new Library();
		
		/*
		 * Here we return and print all of the items(books and magazines) which we have 
		 * exported from the csv files.
		 */
		System.out.println("-----RETURNING ALL BOOKS/MAGAZINES WITH ALL OF THE DETAILS-----");
		List<Item> itemsList = data.getAllItems();
		DataManagment.printItems(itemsList);
		System.out.println();
		
		System.out.println("-----RETURNING ONLY BOOKS WITH ALL OF THE DETAILS-----");
		List<Book> booksList = data.getAllBooks();
		DataManagment.printBooks(booksList);
		System.out.println();
		
		System.out.println("-----RETURNING ONLY MAGAZINES WITH ALL OF THE DETAILS-----");
		List<Magazine> magazinesList = data.getAllMagazines();
		DataManagment.printMagazines(magazinesList);
		System.out.println();
		

		/*
		 * Finding and returning the item by ISBN number. In the first case we return a
		 * book, in the second case we return a magazine and in the third case we return 
		 * null because item with this particular ISBN number does not exist.
		 */
		System.out.println("-----RETURNING A BOOK WITH EXISTING ISBN NUMBER-----");
		String bookISBNnumber = "2215-0012-5487";
		Item bookItem = data.getItemByISBN(bookISBNnumber);
		DataManagment.printItem(bookItem);
		System.out.println();
		
		System.out.println("-----RETURNING A MAGAZINE WITH EXISTING ISBN NUMBER-----");
		String magazineISBNnumber = "4545-8541-2012";
		Item magazineItem = data.getItemByISBN(magazineISBNnumber);
		DataManagment.printItem(magazineItem);
		System.out.println();
		
		System.out.println("-----RETURNING NULL BECAUSE ITEM WITH THIS ISBN NUMBER DOES NOT EXSIST-----");
		String nullISBNnumber = "4885-8671-2099";
		Item nullItem = data.getItemByISBN(nullISBNnumber);
		DataManagment.printItem(nullItem);
		System.out.println();
		
		
		/*
		 * Finding and returning all of the items of an author by using his email.
		 */
		System.out.println("-----RETURNING ALL ITEMS OF A GIVEN AUTHOR-----");
		String authorEmail = "pr-lieblich@optivo.de";
		List<Item> itemsListOfAuthor = data.getItemsListOfAuthor(authorEmail);
		DataManagment.printItems(itemsListOfAuthor);
		System.out.println();
		
		
		/*
		 * Sorting map on its values(Item) by the title of the items and returning
		 * the list of sorted items.
		 */
		System.out.println("-----SORTING AND RETURNING SORTED LIST-----");
		List<Item> itemsListSorted = data.getAllItemsSorted();
		DataManagment.printItems(itemsListSorted);
		System.out.println();
		
		
	}

}
